package com.example.grafian;

 import android.content.Intent;
 import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
 import android.view.View;
 import android.view.Window;
 import android.view.WindowManager;
 import android.widget.Button;

public class home extends AppCompatActivity {
    Button hm;
    Button erp;
    Button add;
    Button link;
    Button chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        hm = (Button) findViewById(R.id.home);
        hm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(home.this, home.class);
                startActivity(i);
            }
        });

        erp = (Button) findViewById(R.id.erp);
        erp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(home.this, home.class);
                startActivity(i2);
            }
        });

        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(home.this, home.class);
                startActivity(i2);
            }
        });

        link = (Button) findViewById(R.id.link);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(home.this, home.class);
                startActivity(i2);
            }
        });

        chat = (Button) findViewById(R.id.chat);
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(home.this, home.class);
                startActivity(i2);
            }
        });

    }
}